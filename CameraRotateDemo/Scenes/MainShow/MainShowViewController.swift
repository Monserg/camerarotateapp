//
//  MainShowViewController.swift
//  CameraRotateDemo
//
//  Created by msm72 on 16.01.2018.
//  Copyright © 2018 msm72. All rights reserved.
//

import UIKit

class MainShowViewController: UIViewController {
    // MARK: - Class Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    // MARK: - Actions
    @IBAction func handlerStartButtonTap(_ sender: UIButton) {
        print("Start button taped.")
    }
}
